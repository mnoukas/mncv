﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AlertModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';
import { EducationComponent } from './components/education/education.component';
import { SchoolingsComponent } from './components/schoolings/schoolings.component';
import { WorkexperienceComponent } from './components/workexperience/workexperience.component';
import { SkillsComponent } from './components/skills/skills.component';
import { ExtrainfoComponent } from './components/extrainfo/extrainfo.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    EducationComponent,
    SchoolingsComponent,
    WorkexperienceComponent,
    SkillsComponent,
    ExtrainfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
