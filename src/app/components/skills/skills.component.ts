import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  languages: any[];
  skills: any[];
  otherSkills: any[];

  constructor() {
    this.languages = [{
      language: 'Estonian',
      ability: 'Native language'
    }, {
      language: 'English',
      ability: 'Full professional proficiency'
    }, {
      language: 'Russian',
      ability: 'Basic'
    }];
    this.skills = ['Java', 'C#', 'React', 'Vue', 'GraphQL', 'Nuxt/Next.js (Server side rendering)', 'AngularJS and Angular 2', 'HTML', 'CSS/SCSS', 'JavaScript',
      'PHP', 'MySQL', 'Bootstrap', 'JQuery', 'Swift', 'Python'];
    this.otherSkills = ['Teamwork', 'Reaper', 'Ableton', 'GarageBand', 'GraphiQL', 'Some photoshop'];
  };

  ngOnInit() {
  }

}
