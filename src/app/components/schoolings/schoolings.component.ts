import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-schoolings',
  templateUrl: './schoolings.component.html',
  styleUrls: ['./schoolings.component.css']
})
export class SchoolingsComponent implements OnInit {
  schoolings: any[];

  constructor() {
    this.schoolings = [
      {
        description: 'Programming robots with Python, iOS app development,' +
        'UX/UI design and Javascript. Also a basic Java course @ Mektory',
        year: '2015 - 2018'
      },
      {
        year: '2009 - 2011',
        description: 'Drums, Ivo Varts'
      },
      {
        year: '2007 - 2009',
        description: 'Electrical guitar, Toomas Vanem'
      },
      {
        year: '2013 - Now',
        description: 'Learning piano on my own'
      }
    ];
  }

  ngOnInit() {
  }

}
