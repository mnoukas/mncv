﻿import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name: string;
  phoneNumber: string;
  email: string;
  address: string;
  educationLevel: string;
  info: any[];

  constructor() {
    this.info = [
      this.name = 'Name: Martin Nõukas',
      this.phoneNumber = 'Number: +3725090693',
      this.email = 'E-mail: noukas.martin@gmail.com',
      this.address = 'Address: Kaluri Tee 8-26, Viimsi, Haabneeme, Estonia',
      this.educationLevel = 'Education: Applied Higher Education degree in IT Systems Development.',
    ];
  };

  ngOnInit() {

  };
}

