import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-extrainfo',
  templateUrl: './extrainfo.component.html',
  styleUrls: ['./extrainfo.component.css']
})
export class ExtrainfoComponent implements OnInit {
  strongPoints: string[];
  weakPoints: string[];
  hobbies: string[];
  driversLicense: boolean;
  linkedInProfile: string;

  constructor() {
    this.strongPoints = [
      ' Great communicative skills', ' Positive outlook on life', 'Honest and helpful towards others',
      'Fast learner', 'Friendly and hard-working', 'Always excited to try out new technologies/frameworks etc',
      'A motivator'
    ];
    this.weakPoints = [
      'Weak russian proficiency',
    ];
    this.hobbies = [
      'Web Design', 'Piano, drums, guitar and singing', 'Going to the gym', 'Volleyball and ping-pong',
      'Recording and writing musical pieces'
    ];
    this.driversLicense = true;
    this.linkedInProfile = 'Click me :)'
  }

  ngOnInit() {
  }

}
