import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  educations: any[];

  constructor() {
    this.educations = [
      {
        year: '2002 - 2014',
        school: 'Viimsi School. (Finished with "good and very good grades" letter)'
      },
      {
        year: '2015 - 2018',
        school: 'Tallinn University of Technology, Infotechnology College. Finished with Applied Higher Education degree in Infotechnology Systems Development.'
      },
      {
        year: '2003 - 2006',
        school: 'Viimsi Music School. (Classical guitar)'
      }
    ];
  }

  ngOnInit() {
  }

}
