import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-workexperience',
  templateUrl: './workexperience.component.html',
  styleUrls: ['./workexperience.component.css']
})
export class WorkexperienceComponent implements OnInit {
  jobs: any[];

  constructor() {
    this.jobs = [
      {
        id: 1, year: '2015', month: 'July - August',
        jobName: 'Bartender at a Viimsi beach bar'
      },
      {
        id: 2, year: '2016', month: 'June - September',
        jobName: 'Making E-schooling courses for Swedbank (Internship)'
      },
      {
        id: 3, year: '2017', month: 'July - September',
        jobName: 'Front-end developer intern at Net Group'
      },
      {
        id: 4, year: '2017', month: 'September - January 2018',
        jobName: 'Junior Front-end developer at Net Group'
      },
      {
        id: 5, year: '2018', month: 'January - Now',
        jobName: 'Full Stack Developer at Singleton'
      }
    ]
  }

  ngOnInit() {
  }

}
