import { NetGroupCvPage } from './app.po';

describe('net-group-cv App', function() {
  let page: NetGroupCvPage;

  beforeEach(() => {
    page = new NetGroupCvPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
